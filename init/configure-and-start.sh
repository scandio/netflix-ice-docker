#!/bin/bash

set -e

# Set default environment variables
export GRAILS_SERVER_URL=${GRAILS_SERVER_URL:=http://localhost:8080/}

# Print used environment variables
echo "Using environment variables:"
echo "---------------------------------------------------------"
echo "> GRAILS_SERVER_URL=${GRAILS_SERVER_URL}"
echo "> GRAILS_SERVER_HEAP_SPACE=${GRAILS_SERVER_HEAP_SPACE}"
echo

echo "Setting grails.serverURL in /opt/ice/grails-app/conf/Config.groovy"
sed -i "s|http://localhost:8080/|${GRAILS_SERVER_URL}|" /opt/ice/grails-app/conf/Config.groovy
echo

echo "Setting -Xmx (Heap Space) in /opt/ice/grailsw"
sed -i "s|-Xmx768M|-Xmx${GRAILS_SERVER_HEAP_SPACE}|" /opt/ice/grailsw
echo

echo "Starting Netflix Ice:"
echo "---------------------------------------------------------"
exec /opt/ice/grailsw ${ICE_ARGUMENTS}

